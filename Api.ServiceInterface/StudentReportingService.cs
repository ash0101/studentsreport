﻿using Business.IService;
using Business.Service.Models;
using ServiceStack;
using System.Net;

namespace Api.ServiceInterface
{
    public class StudentReportingService : Service
    {
        private readonly IStudentService _studentService;

        public StudentReportingService(IStudentService studentService)
        {
            _studentService = studentService;
        }

        public object Get(GetStudents request)
        {
            if (request.Id == default)
            {
                var result = new HttpResult(_studentService.GetStudents());
                return result;
            }
            else
            {
                var student = _studentService.GetStudent(request.Id);

                if (student != null)
                {
                    return new HttpResult(student);
                }
                else
                {
                    return new HttpError(HttpStatusCode.NotFound, "Student with id " + request.Id + " doesn't exist.");
                }
            }
        }

        public object Post(AddStudent student)
        {
            return _studentService.AddStudent(student);
        }

        public object Put(UpdateStudent student)
        {
            return _studentService.UpdateStudent(student);
        }

        public bool Delete(DeleteStudent students)
        {
            return _studentService.DeleteStudent(students.StudentId);
        }
    }
}
