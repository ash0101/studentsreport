﻿using StudentReporting.App_Start;
using System;
using System.Web;

namespace StudentReporting
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            new AppHost().Init();
        }
    }
}