﻿using Api.ServiceInterface;
using Business.IService;
using Business.Service;
using Database.IRepository;
using Database.Repository;
using Database.Schema;
using Funq;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace StudentReporting.App_Start
{
    public class AppHost : AppHostBase
    {
        public AppHost() : base("Student report Service", typeof(StudentReportingService).Assembly) { }
        public override void Configure(Container container)
        {
            var ormLiteConnectionFactory = new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["studentDbConn"].ConnectionString,
                                                                            ServiceStack.OrmLite.SqlServer.SqlServerOrmLiteDialectProvider.Instance);

            container.Register<IDbConnectionFactory>(ormLiteConnectionFactory);

            // Registering Database Layer
            container.RegisterAutoWiredAs<StudentRepository, IStudentRepository>().ReusedWithin(ReuseScope.Container);

            // Registering Business Layer
            container.RegisterAutoWiredAs<StudentService, IStudentService>().ReusedWithin(ReuseScope.Container);

            DbInitializer.InitializeDb(ormLiteConnectionFactory);
        }
    }
}