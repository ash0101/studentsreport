﻿using Api.ServiceInterface;
using Business.IService;
using Business.Service;
using Business.Service.Models;
using Database.IRepository;
using Database.Repository;
using Database.Schema;
using Funq;
using NUnit.Framework;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;

namespace Api.UnitTest
{
    [TestFixture]
    public class IntegrationTests
    {
        const string BaseUri = "http://localhost:2000/";
        private readonly ServiceStackHost appHost;
        readonly Exception testFixtureSetupException = null;

        class AppHost : AppSelfHostBase
        {
            public AppHost() : base(nameof(IntegrationTests), typeof(StudentReportingService).Assembly) { }

            public override void Configure(Container container)
            {
                var ormLiteConnectionFactory = new OrmLiteConnectionFactory("Data Source=(LocalDB)\\MSSQLLocalDB; AttachDbFilename=|DataDirectory|App_Data\\student.mdf; Integrated Security=True",
                                                                            ServiceStack.OrmLite.SqlServer.SqlServerOrmLiteDialectProvider.Instance);

                container.Register<IDbConnectionFactory>(ormLiteConnectionFactory);

                // Registering Database Layer
                container.RegisterAutoWiredAs<StudentRepository, IStudentRepository>().ReusedWithin(ReuseScope.Container);

                // Registering Business Layer
                container.RegisterAutoWiredAs<StudentService, IStudentService>().ReusedWithin(ReuseScope.Container);

                DbInitializer.InitializeDb(ormLiteConnectionFactory);
            }
        }


        public IntegrationTests()
        {
            try
            {
                appHost = new AppHost()
               .Init()
               .Start(BaseUri);
            }
            catch (Exception ex)
            {
                testFixtureSetupException = ex;
            }
        }

        [SetUp]
        public void CheckForTestFixturefailure()
        {
            if (testFixtureSetupException != null)
            {
                string msg = string.Format("{0}{1}{2}{3}",
                    Environment.NewLine, testFixtureSetupException.GetType(),testFixtureSetupException.Message, testFixtureSetupException.StackTrace);
                Assert.Fail(msg);
            }
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //Dispose it on TearDown
            appHost.Dispose();
        }

        [Test]
        public void Add_Student()
        {
            var client = new JsonServiceClient(BaseUri);

            // arrange
            var response = client.Post(new AddStudent
            {
                Name = "Asraful Haque",
                City = "Chittagong",
                CurrentClass = 3
            });

            // assert
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void Get_All_Students()
        {
            var client = new JsonServiceClient(BaseUri);

            // arrange
            var response = client.Get(new GetStudents());

            // assert
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void GetStudent()
        {
            var client = new JsonServiceClient(BaseUri);

            // arrange
            var response = client.Get(new GetStudents { StudentId = 2 });

            // assert
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void Update_Student()
        {
            var client = new JsonServiceClient(BaseUri);

            // arrange
            var response = client.Put(new UpdateStudent
            {
                StudentId = 3,
                Name = "Ashraful",
                City = "Chittagong",
                CurrentClass = 3
            });

            // assert
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void Delete_Student()
        {
            var client = new JsonServiceClient(BaseUri);

            // arrange
            var response = client.Delete(new DeleteStudent { StudentId = 9 });

            // assert
            Assert.That(response, Is.Not.Null);
        }
    }
}
